### Metric Learning
|model path   | Efficiency  | Purity  | KNN  | radius  | graph size |
|---|---|---|---|---| --- |
|metric_learning/best-11292882-f1=0.006355.ckpt   | 99.5  | 2.5  | 1000  | 0.1  | 5e6 |
|   |   |   |   |   | |
|   |   |   |   |   | |

### Filter